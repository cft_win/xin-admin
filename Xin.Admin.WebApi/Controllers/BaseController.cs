﻿using Microsoft.AspNetCore.Mvc;
using Xin.Infrastructure.Attributes;

namespace Xin.Admin.WebApi
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ValidatePermission]
    public abstract class BaseController: ControllerBase
    {

    }
}
